export const employees = [
  {
    id: 1, name: 'Airi Satou', position: 'Accountant',
    office: 'Tokyo', age: 33, startDate: '2008/11/28', salary: '$162,700'
  },

  {
    id: 2,
    name: 'Angelica Ramos',
    position: 'Chief Executive Officer (CEO)',
    office: 'London',
    age: 47,
    startDate: '09.10.2009',
    salary: '$1,200,000'
  },

  {
    id: 3,
    name: 'Ashton Cox',
    position: 'Junior Technical Author',
    office: 'San Francisco',
    age: 66,
    startDate: '12.01.2009',
    salary: '$86,000'
  },

  {id: 4, name: 'Bradley Greer', position: 'Software Engineer', office: 'London', age: 41, startDate: '13.10.2012', salary: '$132,000'},
  {
    id: 5,
    name: 'Brenden Wagner',
    position: 'Software Engineer',
    office: 'San Francisco',
    age: 28,
    startDate: '07.06.2011',
    salary: '$206,850'
  },
  {
    id: 6,
    name: 'Brielle Williamson',
    position: 'Integration Specialist',
    office: 'New York',
    age: 61,
    startDate: '02.12.2012',
    salary: '$372,000'
  },
  {id: 7, name: 'Bruno Nash', position: 'Software Engineer', office: 'London', age: 38, startDate: '03.05.2011', salary: '$163,500'},
  {id: 8, name: 'Caesar Vance', position: 'Pre-Sales Support', office: 'New York', age: 21, startDate: '12.12.2011', salary: '$106,450'},
  {id: 9, name: 'Cara Stevens', position: 'Sales Assistant', office: 'New York', age: 46, startDate: '06.12.2011', salary: '$145,600'},
  {
    id: 10,
    name: 'Cedric Kelly',
    position: 'Senior Javascript Developer',
    office: 'Edinburgh',
    age: 22,
    startDate: '29.03.2012',
    salary: '$433,060'
  },
  {
    id: 11,
    name: 'Charde Marshall',
    position: 'Regional Director',
    office: 'San Francisco',
    age: 36,
    startDate: '16.10.2008',
    salary: '$470,600'
  },
  {
    id: 12,
    name: 'Colleen Hurst',
    position: 'Javascript Developer',
    office: 'San Francisco',
    age: 39,
    startDate: '15.09.2009',
    salary: '$205,500'
  },
  {id: 13, name: 'Dai Rios', position: 'Personnel Lead', office: 'Edinburgh', age: 35, startDate: '26.09.2012', salary: '$217,500'},
  {id: 14, name: 'Donna Snider', position: 'Customer Support', office: 'New York', age: 27, startDate: '25.01.2011', salary: '$112,000'},
  {id: 15, name: 'Doris Wilder', position: 'Sales Assistant', office: 'Sidney', age: 23, startDate: '20.09.2010', salary: '$85,600'},
  {
    id: 16,
    name: 'Finn Camacho',
    position: 'Support Engineer',
    office: 'San Francisco',
    age: 47,
    startDate: '07.07.2009',
    salary: '$87,500'
  },
  {
    id: 17,
    name: 'Fiona Green',
    position: 'Chief Operating Officer (COO)',
    office: 'San Francisco',
    age: 48,
    startDate: '11.03.2010',
    salary: '$850,000'
  },
  {id: 18, name: 'Garrett Winters', position: 'Accountant', office: 'Tokyo', age: 63, startDate: '25.07.2011', salary: '$170,750'},
  {id: 19, name: 'Gavin Cortez', position: 'Team Leader', office: 'San Francisco', age: 22, startDate: '26.10.2008', salary: '$235,500'},
  {id: 20, name: 'Gavin Joyce', position: 'Developer', office: 'Edinburgh', age: 42, startDate: '22.12.2010', salary: '$92,575'},
  {
    id: 21,
    name: 'Gloria Little',
    position: 'Systems Administrator',
    office: 'New York',
    age: 59,
    startDate: '10.04.2009',
    salary: '$237,500'
  },
  {
    id: 22,
    name: 'Haley Kennedy',
    position: 'Senior Marketing Designer',
    office: 'London',
    age: 43,
    startDate: '18.12.2012',
    salary: '$313,500'
  },
  {id: 23, name: 'Hermione Butler', position: 'Regional Director', office: 'London', age: 47, startDate: '21.03.2011', salary: '$356,250'},
  {
    id: 24,
    name: 'Herrod Chandler',
    position: 'Sales Assistant',
    office: 'San Francisco',
    age: 59,
    startDate: '06.08.2012',
    salary: '$137,500'
  },
  {id: 25, name: 'Hope Fuentes', position: 'Secretary', office: 'San Francisco', age: 41, startDate: '12.02.2010', salary: '$109,850'},
  {
    id: 26,
    name: 'Howard Hatfield',
    position: 'Office Manager',
    office: 'San Francisco',
    age: 51,
    startDate: '16.12.2008',
    salary: '$164,500'
  },
  {id: 27, name: 'Jackson Bradshaw', position: 'Director', office: 'New York', age: 65, startDate: '26.09.2008', salary: '$645,750'},
  {id: 28, name: 'Jena Gaines', position: 'Office Manager', office: 'London', age: 30, startDate: '19.12.2008', salary: '$90,560'},
  {
    id: 29,
    name: 'Jenette Caldwell',
    position: 'Development Lead',
    office: 'New York',
    age: 30,
    startDate: '03.09.2011',
    salary: '$345,000'
  },
  {
    id: 30,
    name: 'Jennifer Acosta',
    position: 'Junior Javascript Developer',
    office: 'Edinburgh',
    age: 43,
    startDate: '01.02.2013',
    salary: '$75,650'
  },
  {
    id: 31,
    name: 'Jennifer Chang',
    position: 'Regional Director',
    office: 'Singapore',
    age: 28,
    startDate: '14.11.2010',
    salary: '$357,650'
  },
  {id: 32, name: 'Jonas Alexander', position: 'Developer', office: 'San Francisco', age: 30, startDate: '14.07.2010', salary: '$86,500'},
  {id: 33, name: 'Lael Greer', position: 'Systems Administrator', office: 'London', age: 21, startDate: '27.02.2009', salary: '$103,500'},
  {
    id: 34,
    name: 'Martena Mccray',
    position: 'Post-Sales support',
    office: 'Edinburgh',
    age: 46,
    startDate: '09.03.2011',
    salary: '$324,050'
  },
  {
    id: 35,
    name: 'Michael Bruce',
    position: 'Javascript Developer',
    office: 'Singapore',
    age: 29,
    startDate: '27.06.2011',
    salary: '$183,000'
  },
  {id: 36, name: 'Michael Silva', position: 'Marketing Designer', office: 'London', age: 66, startDate: '27.11.2012', salary: '$198,500'},
  {
    id: 37,
    name: 'Michelle House',
    position: 'Integration Specialist',
    office: 'Sidney',
    age: 37,
    startDate: '02.06.2011',
    salary: '$95,400'
  },
  {id: 38, name: 'Olivia Liang', position: 'Support Engineer', office: 'Singapore', age: 64, startDate: '03.02.2011', salary: '$234,500'},
  {
    id: 39,
    name: 'Paul Byrd',
    position: 'Chief Financial Officer (CFO)',
    office: 'New York',
    age: 64,
    startDate: '09.06.2010',
    salary: '$725,000'
  },
  {id: 40, name: 'Prescott Bartlett', position: 'Technical Author', office: 'London', age: 27, startDate: '07.05.2011', salary: '$145,000'},
  {id: 41, name: 'Quinn Flynn', position: 'Support Lead', office: 'Edinburgh', age: 22, startDate: '03.03.2013', salary: '$342,000'},
  {
    id: 42,
    name: 'Rhona Davidson',
    position: 'Integration Specialist',
    office: 'Tokyo',
    age: 55,
    startDate: '14.10.2010',
    salary: '$327,900'
  },
  {id: 43, name: 'Sakura Yamamoto', position: 'Support Engineer', office: 'Tokyo', age: 37, startDate: '19.08.2009', salary: '$139,575'},
  {id: 44, name: 'Serge Baldwin', position: 'Data Coordinator', office: 'Singapore', age: 64, startDate: '09.04.2012', salary: '$138,575'},
  {id: 45, name: 'Shad Decker', position: 'Regional Director', office: 'Edinburgh', age: 51, startDate: '13.11.2008', salary: '$183,000'},
  {id: 46, name: 'Shou Itou', position: 'Regional Marketing', office: 'Tokyo', age: 20, startDate: '14.08.2011', salary: '$163,000'},
  {id: 47, name: 'Sonya Frost', position: 'Software Engineer', office: 'Edinburgh', age: 23, startDate: '13.12.2008', salary: '$103,600'},
  {id: 48, name: 'Suki Burks', position: 'Developer', office: 'London', age: 53, startDate: '22.10.2009', salary: '$114,500'},
  {
    id: 49,
    name: 'Tatyana Fitzpatrick',
    position: 'Regional Director',
    office: 'London',
    age: 19,
    startDate: '17.03.2010',
    salary: '$385,750'
  },
  {id: 50, name: 'Thor Walton', position: 'Developer', office: 'New York', age: 61, startDate: '11.08.2013', salary: '$98,540'},
  {id: 51, name: 'Tiger Nixon', position: 'System Architect', office: 'Edinburgh', age: 61, startDate: '25.04.2011', salary: '$320,800'},
  {id: 52, name: 'Timothy Mooney', position: 'Office Manager', office: 'London', age: 37, startDate: '11.12.2008', salary: '$136,200'},
  {
    id: 53,
    name: 'Unity Butler',
    position: 'Marketing Designer',
    office: 'San Francisco',
    age: 47,
    startDate: '09.12.2009',
    salary: '$85,675'
  },
  {
    id: 54,
    name: 'Vivian Harrell',
    position: 'Financial Controller',
    office: 'San Francisco',
    age: 62,
    startDate: '14.02.2009',
    salary: '$452,500'
  },
  {
    id: 55,
    name: 'Yuri Berry',
    position: 'Chief Marketing Officer (CMO)',
    office: 'New York',
    age: 40,
    startDate: '25.06.2009',
    salary: '$675,000'
  },
  {id: 56, name: 'Zenaida Frank', position: 'Software Engineer', office: 'New York', age: 63, startDate: '04.01.2010', salary: '$125,250'},
  {
    id: 57,
    name: 'Zorita Serrano',
    position: 'Software Engineer',
    office: 'San Francisco',
    age: 56,
    startDate: '01.06.2012',
    salary: '$115,000'
  },
];
